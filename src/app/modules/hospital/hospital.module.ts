import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HospitalRoutingModule } from './routing/hospital.routes.module';

import { HospitalListComponent } from './components/hospital-list/hospital-list.component';
import { HospitalCreateComponent } from './components/hospital-create/hospital-create.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { MainComponent } from './components/main/main.component';
import { AllHospitalService} from './components/services/all.hospitals.service';
import { DoctorModule } from '../doctor/doctor.module';
import { PatientListForHospitalComponent } from './components/patient-list-for-hospital/patient-list-for-hospital.component';
import { AddPatientHospitalService } from './components/services/add.patient.hospital.service';
import { DoctorListForHospitalComponent } from './components/doctor-list-for-hospital/doctor-list-for-hospital.component';
import { AllDoctorsService } from '../doctor/services/all.doctors.service';
import { AddDoctorHospitalService } from './components/services/add.doctor.hospital.service';
import { HospitalUpdateComponent } from './components/hospital-update/hospital-update.component';
import { UpdateHospitalService } from './components/services/update.hospital.service';
import { DeleteHospitalService } from './components/services/delete.hospital.service';
import { MultiSelectModule } from 'primeng/multiselect';
import { CreateHospitalService } from './components/services/create.hospital.service';

@NgModule({
  declarations: [DoctorListForHospitalComponent, HospitalListComponent, HospitalCreateComponent, MainComponent, PatientListForHospitalComponent, HospitalUpdateComponent],
  imports: [
    CommonModule,
    SharedModule,
    HospitalRoutingModule,
    FormsModule,
    DoctorModule,
    MultiSelectModule
  ],
  providers: [AllHospitalService, AddPatientHospitalService, AllDoctorsService, AddDoctorHospitalService, UpdateHospitalService, DeleteHospitalService, CreateHospitalService]
})
export class HospitalModule { }
