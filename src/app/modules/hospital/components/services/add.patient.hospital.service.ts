import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class AddPatientHospitalService extends AppService{
    constructor(private _httpClient: HttpClient) {
        super('hospitals/add-patients', _httpClient);
    }

    public addPatientsHospital(body: any): Observable<any> {
        return this.httpClient.put(this.url, body);
    }
}