import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class CreateHospitalService extends AppService{
    constructor(private _httpClient:HttpClient){
        super('hospitals', _httpClient);
    }

    public createHospital(body: any): Observable<any> {
        return this.httpClient.post(this.url, body);
    }
}