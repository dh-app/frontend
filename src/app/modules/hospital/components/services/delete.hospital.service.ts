import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class DeleteHospitalService extends AppService {
    constructor(private _httpClient:HttpClient){
        super('hospitals', _httpClient);
    }

    public deleteHospital(hospitalId: number): Observable<any> {
        return this.httpClient.delete(`${this.url}/?hospitalId=${hospitalId}`);
    }
}