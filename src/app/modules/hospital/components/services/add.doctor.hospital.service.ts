import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class AddDoctorHospitalService extends AppService{
    constructor(private _httpClient: HttpClient){
        super('hospitals/add-doctor', _httpClient);
    }

    public addDoctorToHospital(body): Observable<any> {
        return this.httpClient.put(this.url, body);
    }
}