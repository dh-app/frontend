import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class AllHospitalService extends AppService{
    constructor(private _httpClient: HttpClient){
        super('hospitals', _httpClient);
    }

    public allHospitals(): Observable<any> {
        return this.httpClient.get(this.url);
    }
}