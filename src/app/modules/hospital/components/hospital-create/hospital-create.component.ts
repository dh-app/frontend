import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { Subscription } from 'rxjs';
import { PDoctorModel } from 'src/app/modules/doctor/models/PDoctorModel';
import { AllDoctorsService } from 'src/app/modules/doctor/services/all.doctors.service';
import { PPatientsModel } from 'src/app/modules/patient/model/PPatientsModel';
import { AllPatientsService } from 'src/app/modules/patient/services/all.patients.service';
import { StringTrimLeftRight } from 'src/app/modules/shared/services/stringTrimLeftRight';
import { CreateHospitalService } from '../services/create.hospital.service';

@Component({
  selector: 'app-hospital-create',
  templateUrl: './hospital-create.component.html',
  styleUrls: ['./hospital-create.component.scss']
})
export class HospitalCreateComponent implements OnInit {
  public hospitalForm: FormGroup;
  private subscription: Subscription;

  public doctorsFormat: SelectItem[];
  public patientsFormat: SelectItem[];

  public patients: PPatientsModel[];
  public doctors: PDoctorModel[];

  constructor(private _formBuilder: FormBuilder,
              private _allPatientService: AllPatientsService,
              private _allDoctorService: AllDoctorsService,
              private _sanatizeString: StringTrimLeftRight,
              private _createHospitalService: CreateHospitalService) { 
    this._buildForm();
    this._getDoctors();
    this._getPatients();
  }

  private _getPatients(): void {
    this.subscription = this._allPatientService.getAllPatients().subscribe(res => {
      this.patients = res;
      this.patientsFormat = [];
      this.patients.forEach(patient => {
        this.patientsFormat.push(
          {
            label: `${patient.name} ${patient.lastName}`,
            value: patient.id
          }
        );
      });
    });
  }
  private _getDoctors(): void {
    this.subscription = this._allDoctorService.getAllDoctors().subscribe(res => {
      this.doctors = res;
      this.doctorsFormat = [];
      this.doctors.forEach(doctor => {
        this.doctorsFormat.push(
          {
            label: `${doctor.name} ${doctor.lastName}`,
            value: doctor.id
          }
        );
      });
    });
  }

  ngOnInit() {
  }

  private _buildForm(): void {
    this.hospitalForm = this._formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      doctors: ['', Validators.required],
      patients: ['', Validators.required]
    });
  }

  public onSubmit(): void {
    (this.hospitalForm.valid) ? this._subscribe() : alert('Incorrect values');
  }

  private _subscribe(): void {
    let body = {
      name: this._sanatizeString.sanitizeString(this.hospitalForm.get("name").value),
      patients: this.hospitalForm.get("patients").value,
      doctors: this.hospitalForm.get("doctors").value,
      user: "USER-HARDCODE"
    };
    this.subscription = this._createHospitalService.createHospital(body).subscribe(() => {
      alert('Correct');
    }, () => {
      alert('Error');
    });
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

  public inputValidator(controlName: string): boolean {
    return this.hospitalForm.get(controlName).invalid && (this.hospitalForm.get(controlName).touched || this.hospitalForm.get(controlName).dirty);
  }

  public inputRequiredValidator(controlName: string): boolean {
    return this.hospitalForm.get(controlName).errors.required;
  }

  public inputMaxLengthValidator(controlName: string): boolean {
    return this.hospitalForm.get(controlName).errors.maxlength;
  }

  public get formValidator(): boolean {
    return this.hospitalForm.valid === false;
  }
}
