import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PDoctorModel } from 'src/app/modules/doctor/models/PDoctorModel';
import { DPatientModel } from 'src/app/modules/patient/model/DPatientModel';
import { HospitalModel } from '../../models/HospitalModel';
import { AllHospitalService } from '../services/all.hospitals.service';
import { DeleteHospitalService } from '../services/delete.hospital.service';

@Component({
  selector: 'app-hospital-list',
  templateUrl: './hospital-list.component.html',
  styleUrls: ['./hospital-list.component.scss']
})
export class HospitalListComponent implements OnInit {

  public hospitals: HospitalModel[];
  public doctors: PDoctorModel[];
  public patients: DPatientModel[];
  
  public showDoctors: boolean;
  public showPatients: boolean;
  public showUpdateHospital: boolean;
  public showAddDoctor: boolean;
  public showAddPatient: boolean;
  public hospitalId: number;
  public hospital: HospitalModel;

  private subscription: Subscription;

  constructor(private _allHospitalService: AllHospitalService,
              private _deleteHospitalService: DeleteHospitalService) { }

  ngOnInit(): void {
    this._getAllHospitals();
  }

  private _getAllHospitals(): void {
    this.subscription = this._allHospitalService.allHospitals().subscribe(res => {
      this.hospitals = res;
    }, err => {
      console.log(err);
    });
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

  public showDoctorsHospital(doctors: PDoctorModel[]): void {
    this.doctors = doctors;
    this.showDoctors = true;
  }

  public showPatientsHospital(patients: DPatientModel[]): void {
    this.patients = patients;
    this.showPatients = true;
  }

  public showAddPatientHospital(hospitalId: number): void {
    this.hospitalId = hospitalId;
    this.showAddPatient = true;
  }

  public showAddDoctorHospital(hospitalId: number): void {
    this.hospitalId = hospitalId;
    this.showAddDoctor = true;
  }

  public showUpdateHospitalPanel(hospital: HospitalModel): void {
    this.hospital = hospital;
    this.showUpdateHospital = true;
  }

  public closeDialogAddPatient(): void {
    this.hospitalId = null;
    this.showAddPatient = false;
  }

  public closeDialogAddDoctor(): void {
    this.hospitalId = null;
    this.showAddDoctor = false;
  }
  
  public closeDialogUpdateHospital(): void {
    this.hospital = null;
    this.showUpdateHospital = false;
  }

  public deleteHospital(hospitalId: number): void {
    this.subscription = this._deleteHospitalService.deleteHospital(hospitalId).subscribe(() => {
      alert('Correct');
    }, () => {
      alert('Error');
    });
  }
}
