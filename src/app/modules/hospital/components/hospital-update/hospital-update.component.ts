import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { StringTrimLeftRight } from 'src/app/modules/shared/services/stringTrimLeftRight';
import { HospitalModel } from '../../models/HospitalModel';
import { UpdateHospitalService } from '../services/update.hospital.service';

@Component({
  selector: 'app-hospital-update',
  templateUrl: './hospital-update.component.html',
  styleUrls: ['./hospital-update.component.scss']
})
export class HospitalUpdateComponent implements OnInit {
  public updateForm: FormGroup;
  @Input() public hospital: HospitalModel;
  private subscription: Subscription;

  constructor(private _formBuilder: FormBuilder,
    private _sanatizeService: StringTrimLeftRight,
    private _updateHospitalService: UpdateHospitalService) {
    this._buildForm();
  }

  private _buildForm(): void {
    this.updateForm = this._formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]]
    });
  }

  ngOnInit(): void {
    this.updateForm.patchValue({
      name: this.hospital.name
    });
  }

  public onSubmit(): void {
    (this.updateForm.valid) ? this._subscribe() : alert('Incorrect values');
  }

  private _subscribe(): void {
    let body = {
      hospitalId: this.hospital.id,
      name: this._sanatizeService.sanitizeString(this.updateForm.get("name").value),
      user: "USER_HARDCODE"
    }
    this.subscription = this._updateHospitalService.updateHospital(body).subscribe(() => {
      alert('Correct');
    }, err => {
      alert('Error')
    });
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscribe: Subscription): void {
    if (subscribe) {
      subscribe.unsubscribe();
      subscribe = null;
    }
  }

  public inputValidator(controlName: string): boolean {
    return this.updateForm.get(controlName).invalid && (this.updateForm.get(controlName).touched || this.updateForm.get(controlName).dirty);
  }

  public inputRequiredValidator(controlName: string): boolean {
    return this.updateForm.get(controlName).errors.required;
  }

  public inputMaxLengthValidator(controlName: string): boolean {
    return this.updateForm.get(controlName).errors.maxlength;
  }

  public get formValidator(): boolean {
    return this.updateForm.valid === false;
  }
}
