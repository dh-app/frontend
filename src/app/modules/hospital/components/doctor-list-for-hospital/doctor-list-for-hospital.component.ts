import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PDoctorModel } from 'src/app/modules/doctor/models/PDoctorModel';
import { AllDoctorsService } from 'src/app/modules/doctor/services/all.doctors.service';
import { AddDoctorHospitalService } from '../services/add.doctor.hospital.service';

@Component({
  selector: 'app-doctor-list-for-hospital',
  templateUrl: './doctor-list-for-hospital.component.html',
  styleUrls: ['./doctor-list-for-hospital.component.scss']
})
export class DoctorListForHospitalComponent implements OnInit {
  
  @Input() public hospitalId: number;
  private subscription: Subscription;
  public doctors: PDoctorModel[];
  public selectedDoctors: PDoctorModel[];
  
  constructor(private _allDoctorsService: AllDoctorsService,
              private _addDoctorService: AddDoctorHospitalService) { 
                this.selectedDoctors = [];
  }

  ngOnInit() {
    this._allDoctors();
  }

  private _allDoctors(): void {
    this.subscription = this._allDoctorsService.getAllDoctors().subscribe(res => {
      this.doctors = res;
      this.doctors.map(patient => {
        patient.name = `${patient.name} ${patient.lastName}`
      });
    }, err => {
      console.log(err);
    });
  }

  public addDoctors(): void {
    if (this.selectedDoctors.length > 0) {
      let body = {
        hospitalId: this.hospitalId,
        doctors: this._getDoctorsSelected()
      }
      this.subscription = this._addDoctorService.addDoctorToHospital(body).subscribe(() => {
        alert('Correct');
      }, () => {
        alert('Error')
      });
    } else {
      alert('Select a doctor');
    }
  }

  private _getDoctorsSelected(): any {
    let doctors = [];

    this.selectedDoctors.forEach(element => {
      doctors.push(element.id);
    });

    return doctors;
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

}
