import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DPatientModel } from 'src/app/modules/patient/model/DPatientModel';
import { AllPatientsService } from 'src/app/modules/patient/services/all.patients.service';
import { AddPatientHospitalService } from '../services/add.patient.hospital.service';

@Component({
  selector: 'app-patient-list-for-hospital',
  templateUrl: './patient-list-for-hospital.component.html',
  styleUrls: ['./patient-list-for-hospital.component.scss']
})
export class PatientListForHospitalComponent implements OnInit {

  @Input() public hospitalId: number;
  private subscription: Subscription;
  public patients: DPatientModel[];
  public selectedPatients: DPatientModel[];

  constructor(private _allPatientsService: AllPatientsService,
              private _addPatientHospitalService: AddPatientHospitalService) {
    this.selectedPatients = [];
  }

  ngOnInit() {
    this._allPatients();
  }

  private _allPatients(): void {
    this.subscription = this._allPatientsService.getAllPatients().subscribe(res => {
      this.patients = res;
      this.patients.map(patient => {
        patient.name = `${patient.name} ${patient.lastName}`
      });
    }, err => {
      console.log(err);
    });
  }

  public addPatient(): void {
    if (this.selectedPatients.length > 0) {
      let body = {
        hospitalId: this.hospitalId,
        patients: this._getPatientsSelected()
      }
      this.subscription = this._addPatientHospitalService.addPatientsHospital(body).subscribe(() => {
        alert('Correct');
      }, () => {
        alert('Error')
      });
    } else {
      alert('Select a patient');
    }
  }

  private _getPatientsSelected(): any {
    let patients = [];

    this.selectedPatients.forEach(element => {
      patients.push(element.id);
    });

    return patients;
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }
}
