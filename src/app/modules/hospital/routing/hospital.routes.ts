import { Routes } from "@angular/router";
import { NotFoundComponent } from "../../shared/components/not-found/not-found.component";
import { HospitalCreateComponent } from "../components/hospital-create/hospital-create.component";
import { HospitalListComponent } from "../components/hospital-list/hospital-list.component";
import { MainComponent } from "../components/main/main.component";

export const HOSPITAL_ROUTES: Routes = [
    {
        path: '',
        component: MainComponent,
        children: [
            { 
                path: '',
                redirectTo: '/hospitals/list',
                pathMatch: 'full' 
            },
            { 
                path: 'list', 
                component: HospitalListComponent 
            },
            { 
                path: 'create', 
                component: HospitalCreateComponent 
            },
            { 
                path: '**', 
                component: NotFoundComponent 
            }
        ]
    }
];