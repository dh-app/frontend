import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HOSPITAL_ROUTES } from './hospital.routes';

@NgModule({
    imports: [RouterModule.forChild(HOSPITAL_ROUTES)],
    exports: [RouterModule]
})
export class HospitalRoutingModule {}
