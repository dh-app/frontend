import { PDoctorModel } from "../../doctor/models/PDoctorModel";
import { DPatientModel } from "../../patient/model/DPatientModel";

export interface HospitalModel {
    id: number;
    name: string;
    doctors: PDoctorModel[];
    patients: DPatientModel[];
}