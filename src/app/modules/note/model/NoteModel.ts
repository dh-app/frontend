export interface NoteModel {
    id: number;
    description: string;
    createdDate: string;
    updatedDate: string;
    createdBy: string;
    updateBy: string;
}