import { Component, Input, OnInit } from '@angular/core';
import { NoteModel } from '../../model/NoteModel';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss']
})
export class NoteListComponent implements OnInit {

  @Input() public notes: NoteModel[];
  public cols: any[];

  constructor() { }

  ngOnInit() {
    this.cols = [
      {
        field: 'description',
        header: 'Notes'
      },
      {
        field: 'createdDate',
        header: 'Created date'
      }
    ];
  }

}
