import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { StringTrimLeftRight } from 'src/app/modules/shared/services/stringTrimLeftRight';
import { NoteToPatientService } from '../../services/note.add.to.patient.service';
import { NoteCreateService } from '../../services/note.create.service';

@Component({
  selector: 'app-note-create',
  templateUrl: './note-create.component.html',
  styleUrls: ['./note-create.component.scss']
})
export class NoteCreateComponent implements OnInit {
  public createNote: FormGroup;
  @Input() public patientId: number;
  private subscription: Subscription;

  constructor(private _formBuilder: FormBuilder,
              private _sanatizeString: StringTrimLeftRight,
              private _createNoteService: NoteCreateService,
              private _addNoteToPatient: NoteToPatientService) { 
    this.buildForm();
  }

  private buildForm(): void {
    this.createNote = this._formBuilder.group({
      description: ['', [Validators.required, Validators.maxLength(255)]],
      dateNote: ['', [Validators.required]]
    });
  }

  public onSubmit(): void {
    (this.createNote.valid) ? this._subscribe() : alert('Incorrect values');
  }

  private _subscribe(): void {
    this.registerNote()
      .then(note => {
        let noteArr = [];
        noteArr.push(note);
        return noteArr;
      })
      .then(noteArr => {
        let body = {
          patientId: this.patientId,
          notes: noteArr
        }
        this._addNoteToPatient.addNoteToPatient(body).subscribe(patient => {
          alert('Correct register')
        }, (err) => {
          alert('Error');
        });
      })
      .catch(console.log);
  }

  ngOnInit() {
  }

  private registerNote(): Promise<any> {
    let bodyNote = {
      user: "USER_HARDCODE",
      createdDate: this.createNote.get("dateNote").value,
      description: this._sanatizeString.sanitizeString(this.createNote.get("description").value)
    }

    return this._createNoteService.createNote(bodyNote).toPromise();
  }

  public inputValidator(controlName: string): boolean {
    return this.createNote.get(controlName).invalid && (this.createNote.get(controlName).touched || this.createNote.get(controlName).dirty);
  }

  public inputRequiredValidator(controlName: string): boolean {
    return this.createNote.get(controlName).errors.required;
  }

  public inputMaxLengthValidator(controlName: string): boolean {
    return this.createNote.get(controlName).errors.maxlength;
  }

  public get formValidator(): boolean {
    return this.createNote.valid === false;
  }
}
