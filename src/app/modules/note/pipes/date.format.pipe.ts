import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'dateFormat'})
export class DateFormatPipe implements PipeTransform {
    transform(value: string): string {
        let date = value;
        date = date.replace("T00:", " ");
        date = date.replace("+0000", "");
        date = date.substr(0, 16);

        return date;
    }
}