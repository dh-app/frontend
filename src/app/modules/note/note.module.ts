import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoteListComponent } from './components/note-list/note-list.component';
import { NoteCreateComponent } from './components/note-create/note-create.component';
import { SharedModule } from '../shared/shared.module';
import { NoteCreateService } from './services/note.create.service';
import { NoteToPatientService } from './services/note.add.to.patient.service';
import { DateFormatPipe } from './pipes/date.format.pipe';

@NgModule({
  declarations: [DateFormatPipe, NoteListComponent, NoteCreateComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [NoteListComponent, NoteCreateComponent],
  providers: [NoteCreateService, NoteToPatientService]
})
export class NoteModule { }
