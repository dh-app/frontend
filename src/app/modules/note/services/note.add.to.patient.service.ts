import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class NoteToPatientService extends AppService {
    constructor(private _httpClient: HttpClient){
        super('patients/add-note', _httpClient);
    }

    public addNoteToPatient(body: any): Observable<any>{
        return this.httpClient.put(this.url, body);
    }
}