import { Routes } from '@angular/router';
import { NotFoundComponent } from '../shared/components/not-found/not-found.component';

export const APP_ROUTES: Routes = [
    {
        path: '',
        redirectTo: '/hospitals/list',
        pathMatch: 'full'
    },
    {
        path: 'hospitals',
        loadChildren: '../hospital/hospital.module#HospitalModule'
    },
    {
        path: 'patients',
        loadChildren: '../patient/patient.module#PatientModule'
    },
    {
        path: 'specialities',
        loadChildren: '../speciality/speciality.module#SpecialityModule'
    },
    {
        path: 'doctors',
        loadChildren: '../doctor/doctor.module#DoctorModule'
    },
    {
        path: '**',
        component: NotFoundComponent
    }
];
