import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class PatientsPaginateService extends AppService{
    constructor(private _httpClient: HttpClient){
        super('patients/search', _httpClient);
    }

    public getPatientsPaginate(limit:number, page:number, name:string = '', lastname:string = ''): Observable<any> {
        return this.httpClient.get(`${this.url}?limit=${limit}&page=${page}&name=${name}&lastName=${lastname}`);
    }
}