import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class CreatePatientService extends AppService{
    constructor(private _httpClient:HttpClient){
        super('patients', _httpClient);
    }
    
    public patientCreate(body: any): Observable<any> {
        return this.httpClient.post(this.url, body);
    }
}