import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class DeletePatientService extends AppService{
    constructor(private _httpClient:HttpClient){
        super('patients', _httpClient);
    }

    public deletePatient(patientId: number): Observable<any> {
        return this.httpClient.delete(`${this.url}?patientId=${patientId}`);
    }
}