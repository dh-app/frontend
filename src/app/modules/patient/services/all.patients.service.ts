import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class AllPatientsService extends AppService{
    constructor(private _httpClient: HttpClient){
        super('patients', _httpClient);
    }

    public getAllPatients(): Observable<any> {
        return this.httpClient.get(this.url);
    }
}