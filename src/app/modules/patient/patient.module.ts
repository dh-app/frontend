import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientListComponent } from './components/patient-list/patient-list.component';
import { PatientCreateComponent } from './components/patient-create/patient-create.component';
import { PatientRoutingModule } from './routing/patient.routes.module';
import { SharedModule } from '../shared/shared.module';
import { AllPatientsService } from './services/all.patients.service';
import { NoteModule } from '../note/note.module';
import { DoctorModule } from '../doctor/doctor.module';
import { PatientUpdateComponent } from './components/patient-update/patient-update.component';
import { UpdatePatientService } from './services/update.patient.service';
import { ToastModule } from 'primeng/toast';
import { DeletePatientService } from './services/delete.patient.service';
import { MainPatientComponent } from './components/main-patient/main-patient.component';
import { CreatePatientService } from './services/create.patient.service';
import { PatientSearchComponent } from './components/patient-search/patient-search.component';
import { PatientItemComponent } from './components/patient-item/patient-item.component';
import { FormsModule } from '@angular/forms';
import { PatientsPaginateService } from './services/patients.paginate.service';

@NgModule({
  declarations: [PatientListComponent, PatientCreateComponent, PatientUpdateComponent, MainPatientComponent, PatientSearchComponent, PatientItemComponent],
  imports: [
    CommonModule,
    PatientRoutingModule,
    SharedModule,
    NoteModule,
    DoctorModule,
    ToastModule,
    FormsModule
  ],
  exports: [],
  providers: [AllPatientsService, UpdatePatientService, DeletePatientService, CreatePatientService, PatientsPaginateService]
})
export class PatientModule { }
