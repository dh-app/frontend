import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { StringTrimLeftRight } from 'src/app/modules/shared/services/stringTrimLeftRight';
import { PPatientsModel } from '../../model/PPatientsModel';
import { UpdatePatientService } from '../../services/update.patient.service';

@Component({
  selector: 'app-patient-update',
  templateUrl: './patient-update.component.html',
  styleUrls: ['./patient-update.component.scss']
})
export class PatientUpdateComponent implements OnInit {
  @Input() public patient: PPatientsModel;

  public updateForm: FormGroup;
  private subscription: Subscription;

  constructor(private _formBuilder: FormBuilder,
              private _sanatizeString: StringTrimLeftRight,
              private _updatePatientService: UpdatePatientService) {
    this.buildForm();
  }

  private buildForm(): void {
    this.updateForm = this._formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      lastName: ['', [Validators.required, Validators.maxLength(255)]],
      address: ['', [Validators.required, Validators.maxLength(255)]],
      dateBirth: ['', [Validators.required]],
      profileImageUrl: ['', [Validators.required]]
    });
  }

  public onSubmit(): void {
    (this.updateForm.valid) ? this._subscribe() : alert('Incorrect values');
  }

  ngOnInit() {
    this.updateForm.patchValue({
      name: this.patient.name,
      lastName: this.patient.lastName,
      address: this.patient.address,
      dateBirth: this.patient.dateBirth,
      profileImageUrl: this.patient.profileImageUrl
    });
  }

  private _subscribe(): void {
    let body = {
      patientId: this.patient.id,
      name: this._sanatizeString.sanitizeString(this.updateForm.get("name").value),
      lastName: this._sanatizeString.sanitizeString(this.updateForm.get("lastName").value),
      address: this._sanatizeString.sanitizeString(this.updateForm.get("address").value),
      dateBirth: this.updateForm.get("dateBirth").value,
      profileImageUrl: this._sanatizeString.sanitizeString(this.updateForm.get("profileImageUrl").value),
      user: "USER_HARDCODE"
    }
    this.subscription = this._updatePatientService.updatePatient(body).subscribe(() => {
      alert('Updated');
    }, () => {
      alert('Error');
    });
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

  public inputValidator(controlName: string): boolean {
    return this.updateForm.get(controlName).invalid && (this.updateForm.get(controlName).touched || this.updateForm.get(controlName).dirty);
  }

  public inputRequiredValidator(controlName: string): boolean {
    return this.updateForm.get(controlName).errors.required;
  }

  public inputMaxLengthValidator(controlName: string): boolean {
    return this.updateForm.get(controlName).errors.maxlength;
  }

  public get formValidator(): boolean {
    return this.updateForm.valid === false;
  }
}
