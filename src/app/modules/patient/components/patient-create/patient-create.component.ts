import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { StringTrimLeftRight } from 'src/app/modules/shared/services/stringTrimLeftRight';
import { CreatePatientService } from '../../services/create.patient.service';

@Component({
  selector: 'app-patient-create',
  templateUrl: './patient-create.component.html',
  styleUrls: ['./patient-create.component.scss']
})
export class PatientCreateComponent implements OnInit {
  public patientForm: FormGroup;
  private subscription: Subscription;

  constructor(private _formBuilder: FormBuilder,
              private _sanatizeString: StringTrimLeftRight,
              private _patientCreateService: CreatePatientService) { 
    this._buildForm();
  }

  ngOnInit() {
  }

  private _buildForm(): void {
    this.patientForm = this._formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      lastName: ['', [Validators.required, Validators.maxLength(255)]],
      address: ['', [Validators.required, Validators.maxLength(255)]],
      dateBirth: ['', Validators.required]
    });
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

  public onSubmit(): void {
    (this.patientForm.valid) ? this._subscribe() : alert('Incorrect values');
  }

  private _subscribe(): void {
    let body = {
      name: this._sanatizeString.sanitizeString(this.patientForm.get("name").value),
      lastName: this._sanatizeString.sanitizeString(this.patientForm.get("lastName").value),
      dateBirth: this._sanatizeString.sanitizeString(this.patientForm.get("dateBirth").value),
      address: this._sanatizeString.sanitizeString(this.patientForm.get("address").value),
      profileImageUrl: "NAME_IMAGE.FORMAT",
      user: "USER_HARDCODE"
    }
    this.subscription = this._patientCreateService.patientCreate(body).subscribe(res => {
      alert('Correct');
    }, () => {
      alert('Error');
    });
  }

  public inputValidator(controlName: string): boolean {
    return this.patientForm.get(controlName).invalid && (this.patientForm.get(controlName).touched || this.patientForm.get(controlName).dirty);
  }

  public inputRequiredValidator(controlName: string): boolean {
    return this.patientForm.get(controlName).errors.required;
  }
  
  public inputMaxLengthValidator(controlName: string): boolean {
    return this.patientForm.get(controlName).errors.maxlength;
  }

  public get formValidator(): boolean {
    return this.patientForm.valid === false;
  }
}
