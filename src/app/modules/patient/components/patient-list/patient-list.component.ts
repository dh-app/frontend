import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PDoctorModel } from 'src/app/modules/doctor/models/PDoctorModel';
import { NoteModel } from 'src/app/modules/note/model/NoteModel';
import { PPatientsModel } from '../../model/PPatientsModel';
import { AllPatientsService } from '../../services/all.patients.service';
import { DeletePatientService } from '../../services/delete.patient.service';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss']
})
export class PatientListComponent implements OnInit {

  public patients: PPatientsModel[];
  public showNotes: boolean;
  public showDoctors: boolean;
  public showUpdatePatient: boolean;
  public showAddNote: boolean;

  public patientsNotes: NoteModel[];
  public doctorsPatient: PDoctorModel[];
  public patientId: number;
  public patient: PPatientsModel;

  private subscription: Subscription;

  constructor(private _allPatientsService: AllPatientsService,
              private _deletePatientService: DeletePatientService) { }

  ngOnInit() {
    this.getAllPatients();
  }

  private getAllPatients(): void {
    this.subscription = this._allPatientsService.getAllPatients().subscribe(res => {
      this.patients = res;
    }, err => {
      console.log(err);
    });
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

  public deletePatient(patientId: number): void {
    this._deletePatientService.deletePatient(patientId).subscribe(() => {
      alert('Deleted');
    }, () => {
      alert('Error');
    });
  }

  public showNotesPatient(notes: NoteModel[]): void {
    this.patientsNotes = notes;
    this.showNotes = true;
  }

  public showDoctorsPatient(doctors: PDoctorModel[]): void {
    this.doctorsPatient = doctors;
    this.showDoctors = true;
  }

  public addNotesPatient(patientId: number): void {
    this.patientId = patientId;
    this.showAddNote = true;
  }

  public updatePatient(patient: PPatientsModel): void {
    this.patient = patient;
    this.showUpdatePatient = true;
  }

  public closeUpdatePatient(){
    this.patient = null;
    this.showUpdatePatient = false;
  }
}
