import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PatientsPaginateService } from '../../services/patients.paginate.service';

@Component({
  selector: 'app-patient-search',
  templateUrl: './patient-search.component.html',
  styleUrls: ['./patient-search.component.scss']
})
export class PatientSearchComponent implements OnInit {
  private subscription: Subscription;
  private limit: number;
  private page: number;
  private data: any;

  public name: string;
  public lastName: string;
  public patients: any[];
  constructor(private _allPatientsPaginate: PatientsPaginateService) { 
    this.limit = 4;
    this.page = 0;
    this.patients = [];
  }

  ngOnInit() {
    this._getPatients();
  }

  private _getPatients(): void {
    this.subscription = this._allPatientsPaginate.getPatientsPaginate(this.limit, this.page, this.name, this.lastName).subscribe(res => {
      this.data = res.content;
      for (let i = 0; i < this.data.length; i++) {
        const element = this.data[i];
        this.patients.push(element);
      }
    });
  }

  public scrolled(): void {
    this.page++;
    this._getPatients();
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

  public searchPatient(): void {
    this.patients = [];
    this.page = 0;
    this._getPatients();
  }
}
