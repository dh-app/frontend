import { PDoctorModel } from "../../doctor/models/PDoctorModel";
import { NoteModel } from "../../note/model/NoteModel";

export interface PPatientsModel {
    id: number;
    name: string;
    lastName: string;
    dateBirth: string;
    address: string;
    profileImageUrl: string;
    notes: NoteModel[];
    doctors: PDoctorModel[];
}