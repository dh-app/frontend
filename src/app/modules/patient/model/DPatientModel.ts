import { NoteModel } from "../../note/model/NoteModel";

export interface DPatientModel {
    id: number;
    name: string;
    lastName: string;
    dateBirth: string;
    address: string;
    profileImageUrl: string;
    notes: NoteModel[];
}