import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { PATIENT_ROUTES } from './patient.routes'

@NgModule({
    imports: [RouterModule.forChild(PATIENT_ROUTES)],
    exports: [RouterModule]
})
export class PatientRoutingModule {}
