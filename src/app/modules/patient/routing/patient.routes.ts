import { Routes } from "@angular/router";
import { NotFoundComponent } from "../../shared/components/not-found/not-found.component";
import { MainPatientComponent } from "../components/main-patient/main-patient.component";
import { PatientCreateComponent } from "../components/patient-create/patient-create.component";
import { PatientListComponent } from "../components/patient-list/patient-list.component";
import { PatientSearchComponent } from "../components/patient-search/patient-search.component";

export const PATIENT_ROUTES: Routes = [
    { 
        path: '', 
        component: MainPatientComponent,
        children: [
            { 
                path: '', 
                redirectTo: '/patients/list',
                pathMatch: 'full' 
            },
            { 
                path: 'list', 
                component: PatientListComponent 
            },
            {
                path: 'create',
                component: PatientCreateComponent
            },
            {
                path: 'search',
                component: PatientSearchComponent
            },
            {
                path: '**',
                component: NotFoundComponent
            }
        ] 
    }
];
