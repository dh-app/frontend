import { SpecialityModel } from "../../speciality/models/SpecialityModel";

export interface PDoctorModel {
    id: number;
    name: string;
    lastName: string;
    dateBirth: string;
    address: string;
    profileImageUrl: string;
    specialities: SpecialityModel[];
}