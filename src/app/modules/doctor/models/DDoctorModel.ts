import { DPatientModel } from "../../patient/model/DPatientModel";
import { SpecialityModel } from "../../speciality/models/SpecialityModel";

export interface DDoctorModel {
    id: number;
    name: string;
    lastName: string;
    dateBirth: string;
    address: string;
    profileImageUrl: string;
    specialities: SpecialityModel[];
    patients: DPatientModel[];
}