import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DPatientModel } from 'src/app/modules/patient/model/DPatientModel';
import { DDoctorModel } from '../../models/DDoctorModel';
import { AllDoctorsService } from '../../services/all.doctors.service';
import { DeleteDoctorService } from '../../services/delete.doctor.service';

@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.scss']
})
export class DoctorListComponent implements OnInit {

  public doctors: DDoctorModel[];
  private subscription: Subscription;

  public patientsM: DPatientModel[];
  public doctorId: number;
  public doctor: DDoctorModel;
  public showPatients: boolean;
  public showToAddPatient: boolean;
  public showEditDoctorPanel: boolean;

  constructor(private _allDoctorsService: AllDoctorsService,  
              private _deleteDoctorService: DeleteDoctorService) { }

  ngOnInit() {
    this.getAllDoctors();
  }

  private getAllDoctors(): void {
    this.subscription= this._allDoctorsService.getAllDoctors().subscribe(res => {
      this.doctors = res;
    }, err => {
      console.log(err);
    });
  }

  public showPatientsModal(patients: any): void {
    this.patientsM = patients;
    this.showPatients = true;
  }

  public showListPattientsToAdd(doctorId: number): void {
    this.doctorId = doctorId;
    this.showToAddPatient = true;
  }
  
  public showEditDoctor(doctor: DDoctorModel): void {
    this.doctor = doctor;
    this.showEditDoctorPanel = true;
  }

  public closeDialogAddPatient(): void {
    this.doctorId = null;
    this.showToAddPatient = false;
  }

  public closeDialogEditDoctor(): void {
    this.doctor = null;
    this.showEditDoctorPanel = false;
  }

  public deleteDoctor(doctorId: number): void {
    this.subscription = this._deleteDoctorService.deleteDoctor(doctorId).subscribe(() => {
      alert('Correct');
    }, () => {
      alert('Error');
    });
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }
}
