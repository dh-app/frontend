import { Component, Input, OnInit } from '@angular/core';
import { DPatientModel } from '../../../patient/model/DPatientModel';

@Component({
  selector: 'app-patient-list-for-doctor',
  templateUrl: './patient-list-for-doctor.component.html',
  styleUrls: ['./patient-list-for-doctor.component.scss']
})
export class PatientListForDoctorComponent implements OnInit {
  @Input() public patients: DPatientModel[];
  public cols: any[];

  constructor() { }

  ngOnInit() {
    this.cols = [
      {
        field: 'name',
        header: 'Name'
      },
      {
        field: 'lastName',
        header: 'Lastname'
      },
      {
        field: 'address',
        header: 'Address'
      },
      {
        field: 'notes',
        header: 'All notes'
      }
    ];
  }

  public isArray(obj: any){
    return Array.isArray(obj);
  }
}
