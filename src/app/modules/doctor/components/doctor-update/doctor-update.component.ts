import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { StringTrimLeftRight } from 'src/app/modules/shared/services/stringTrimLeftRight';
import { DDoctorModel } from '../../models/DDoctorModel';
import { UpdateDoctorService } from '../../services/update.doctor.service';

@Component({
  selector: 'app-doctor-update',
  templateUrl: './doctor-update.component.html',
  styleUrls: ['./doctor-update.component.scss']
})
export class DoctorUpdateComponent implements OnInit {
  @Input() public doctor: DDoctorModel;
  public updateForm: FormGroup;
  private subscription: Subscription;

  constructor(private _formBuilder: FormBuilder,
    private _sanatizeString: StringTrimLeftRight,
    private _updateDoctorService: UpdateDoctorService) {
    this.buildForm();
  }

  ngOnInit() {
    this.updateForm.patchValue({
      name: this.doctor.name,
      lastName: this.doctor.lastName,
      address: this.doctor.address,
      dateBirth: this.doctor.dateBirth,
      profileImageUrl: this.doctor.profileImageUrl
    });
  }
  
  public onSubmit(): void {
    (this.updateForm.valid) ? this._subscribe() : alert('Incorrect values');
  }

  private _subscribe(): void {
    let body = {
      doctorId: this.doctor.id,
      name: this._sanatizeString.sanitizeString(this.updateForm.get("name").value),
      lastName: this._sanatizeString.sanitizeString(this.updateForm.get("lastName").value),
      address: this._sanatizeString.sanitizeString(this.updateForm.get("address").value),
      dateBirth: this.updateForm.get("dateBirth").value,
      profileImageUrl: this._sanatizeString.sanitizeString(this.updateForm.get("profileImageUrl").value),
      user: "USER_HARDCODE"
    }
    this.subscription = this._updateDoctorService.updateDoctor(body).subscribe(res => {
      alert('Correct');
    }, err => {
      alert('Error');
    });
  }

  private buildForm(): void {
    this.updateForm = this._formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      lastName: ['', [Validators.required, Validators.maxLength(255)]],
      address: ['', [Validators.required, Validators.maxLength(255)]],
      dateBirth: ['', [Validators.required]],
      profileImageUrl: ['', [Validators.required]]
    });
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

  public inputValidator(controlName: string): boolean {
    return this.updateForm.get(controlName).invalid && (this.updateForm.get(controlName).touched || this.updateForm.get(controlName).dirty);
  }

  public inputRequiredValidator(controlName: string): boolean {
    return this.updateForm.get(controlName).errors.required;
  }

  public inputMaxLengthValidator(controlName: string): boolean {
    return this.updateForm.get(controlName).errors.maxlength;
  }

  public get formValidator(): boolean {
    return this.updateForm.valid === false;
  }
}
