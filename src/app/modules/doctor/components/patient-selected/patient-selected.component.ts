import { Component, Input, OnInit } from '@angular/core';
import { PPatientsModel } from 'src/app/modules/patient/model/PPatientsModel';
import { AllPatientsService } from 'src/app/modules/patient/services/all.patients.service';
import { AddPatientToDoctorService } from '../../services/add.patient.to.doctor.service';

@Component({
  selector: 'app-patient-selected',
  templateUrl: './patient-selected.component.html',
  styleUrls: ['./patient-selected.component.scss']
})
export class PatientSelectedComponent implements OnInit {
  @Input() public doctorId: number;
  public patients: PPatientsModel[];
  public selectedPatients: PPatientsModel[];

  constructor(private _allPatientsService: AllPatientsService,
    private _addPattientService: AddPatientToDoctorService) {
  }

  ngOnInit() {
    this._allPatients();
  }

  private _allPatients(): void {
    this._allPatientsService.getAllPatients().subscribe(res => {
      this.patients = res;
      this.patients.map(patient => {
        patient.name = `${patient.name} ${patient.lastName}`
      });
    }, err => {
      console.log(err);
    });
  }

  public addPatient(): void {
    if(this.selectedPatients.length > 0){
      let body = {
        doctorId: this.doctorId,
        patients: this._getPatientsSelected()
      }
      this._addPattientService.addPatientToDoctor(body).subscribe(() => {
        alert('Correct');
      }, () => {
        alert('Error');
      });
    } else {
      alert('Select a patient');
    }
  }
  
  private _getPatientsSelected(): any {
    let patients = [];

    this.selectedPatients.forEach(element => {
      patients.push(element.id);
    });

    return patients;
  }

}
