import { Component, Input, OnInit } from '@angular/core';
import { PDoctorModel } from '../../models/PDoctorModel';

@Component({
  selector: 'app-doctors-list-for-patient',
  templateUrl: './doctors-list-for-patient.component.html',
  styleUrls: ['./doctors-list-for-patient.component.scss']
})
export class DoctorsListForPatientComponent implements OnInit {

  @Input() public doctors: PDoctorModel[];
  public cols: any[];

  constructor() { }

  ngOnInit() {
    this.cols = [
      {
        field: 'name',
        header: 'Name'
      },
      {
        field: 'lastName',
        header: 'Lastname'
      },
      {
        field: 'address',
        header: 'Address'
      },
      {
        field: 'specialities',
        header: 'Specialities'
      }
    ];
  }

  public isArray(obj: any){
    return Array.isArray(obj);
  }
}
