import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { Subscription } from 'rxjs';
import { StringTrimLeftRight } from 'src/app/modules/shared/services/stringTrimLeftRight';
import { SpecialityModel } from 'src/app/modules/speciality/models/SpecialityModel';
import { AllListService } from 'src/app/modules/speciality/services/all.list.service';
import { CreateDoctorService } from '../../services/create.doctor.service';

@Component({
  selector: 'app-doctor-create',
  templateUrl: './doctor-create.component.html',
  styleUrls: ['./doctor-create.component.scss']
})
export class DoctorCreateComponent implements OnInit {
  public doctorForm: FormGroup;
  public specialities : SpecialityModel[];
  public selectedSpecialities: SpecialityModel[];
  public specialitiesFormat: SelectItem[];
  private subscription: Subscription;

  constructor(private _formBuilder: FormBuilder,
    private _sanatizeString: StringTrimLeftRight,
    private _allSpecialitiesService: AllListService,
    private _createDoctorService: CreateDoctorService) {
    this._buildForm();
    this._getSpecialities();
  }

  private _getSpecialities(): void {
    this.subscription = this._allSpecialitiesService.listSpecialities().subscribe(res => { 
      this.specialities = res;
      this.specialitiesFormat = [];
      this.specialities.forEach(speciality => {
        this.specialitiesFormat.push(
          {
            label: speciality.name,
            value: speciality
          });
      });
    }, () => {
      alert('Error');
    });
  }

  ngOnInit() {
  }

  private _buildForm(): void {
    this.doctorForm = this._formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      lastName: ['', [Validators.required, Validators.maxLength(255)]],
      address: ['', [Validators.required, Validators.maxLength(255)]],
      dateBirth: ['', Validators.required],
      specialities: ['', Validators.required]
    });
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

  public onSubmit(): void {
    (this.doctorForm.valid) ? this._subscribe() : alert('Incorrect values');
  }

  private _subscribe(): void {
    let body = {
      name: this._sanatizeString.sanitizeString(this.doctorForm.get("name").value),
      lastName: this._sanatizeString.sanitizeString(this.doctorForm.get("lastName").value),
      dateBirth: this._sanatizeString.sanitizeString(this.doctorForm.get("dateBirth").value),
      address: this._sanatizeString.sanitizeString(this.doctorForm.get("address").value),
      specialities: this.doctorForm.get("specialities").value,
      profileImageUrl: "NAME_IMAGE.FORMAT",
      user: "USER_HARDCODE"
    }
    this.subscription = this._createDoctorService.registerDoctor(body).subscribe(() => {
      alert('Correct');
    }, () => {
      alert('Error');
    });
  }

  public inputValidator(controlName: string): boolean {
    return this.doctorForm.get(controlName).invalid && (this.doctorForm.get(controlName).touched || this.doctorForm.get(controlName).dirty);
  }

  public inputRequiredValidator(controlName: string): boolean {
    return this.doctorForm.get(controlName).errors.required;
  }

  public inputMaxLengthValidator(controlName: string): boolean {
    return this.doctorForm.get(controlName).errors.maxlength;
  }

  public get formValidator(): boolean {
    return this.doctorForm.valid === false;
  }
}
