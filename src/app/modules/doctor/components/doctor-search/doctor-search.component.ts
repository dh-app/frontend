import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DoctorPaginateService } from '../../services/doctors.paginate.service';

@Component({
  selector: 'app-doctor-search',
  templateUrl: './doctor-search.component.html',
  styleUrls: ['./doctor-search.component.scss']
})
export class DoctorSearchComponent implements OnInit {
  private subscription: Subscription;
  private limit: number;
  private page: number;
  private data: any;
  public name: string;
  public lastName: string;
  public doctors: any[];

  constructor(private _allDoctorsPaginate: DoctorPaginateService) { 
    this.limit = 4;
    this.page = 0;
    this.doctors = [];
  }

  ngOnInit() {
    this._getDoctorsPaginate();
  }

  private _getDoctorsPaginate(): void {
    this.subscription = this._allDoctorsPaginate.getDoctorsPaginate(this.limit, this.page, this.name, this.lastName).subscribe(res => {
      this.data = res.content;
      for (let i = 0; i < this.data.length; i++) {
        const element = this.data[i];
        this.doctors.push(element);
      }
    });
  }

  public scrolled(): void {
    this.page++;
    this._getDoctorsPaginate();
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

  public searchDoctor(): void {
    this.doctors = [];
    this.page = 0;
    this._getDoctorsPaginate();
  }
}
