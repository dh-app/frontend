import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataViewModule } from 'primeng/dataview';
import { SharedModule } from '../shared/shared.module';
import { DoctorRoutingModule } from './routing/doctor.routing.module';

import { DoctorListComponent } from './components/doctor-list/doctor-list.component';
import { DoctorCreateComponent } from './components/doctor-create/doctor-create.component';
import { ListboxModule } from 'primeng/listbox';

import { AllDoctorsService } from './services/all.doctors.service';
import { DoctorsListForPatientComponent } from './components/doctors-list-for-patient/doctors-list-for-patient.component';
import { MainComponent } from './components/main/main.component';
import { PatientListForDoctorComponent } from './components/patient-list-for-doctor/patient-list-for-doctor.component';
import { PatientSelectedComponent } from './components/patient-selected/patient-selected.component';
import { AllPatientsService } from '../patient/services/all.patients.service';
import { FormsModule } from '@angular/forms';
import { AddPatientToDoctorService } from './services/add.patient.to.doctor.service';
import { DoctorUpdateComponent } from './components/doctor-update/doctor-update.component';
import { UpdateDoctorService } from './services/update.doctor.service';
import { DeleteDoctorService } from './services/delete.doctor.service';
import { AllListService } from '../speciality/services/all.list.service';
import { MultiSelectModule } from 'primeng/multiselect';
import { CreateDoctorService } from './services/create.doctor.service';
import { DoctorSearchComponent } from './components/doctor-search/doctor-search.component';
import { DoctorPaginateService } from './services/doctors.paginate.service';
import { DoctorItemComponent } from './components/doctor-item/doctor-item.component';

@NgModule({
  declarations: [PatientListForDoctorComponent, DoctorListComponent, DoctorCreateComponent, DoctorsListForPatientComponent, MainComponent, PatientSelectedComponent, DoctorUpdateComponent, DoctorSearchComponent, DoctorItemComponent],
  imports: [
    CommonModule,
    DoctorRoutingModule,
    SharedModule,
    DataViewModule,
    FormsModule,
    MultiSelectModule
  ],
  exports: [DoctorsListForPatientComponent, DoctorsListForPatientComponent, PatientListForDoctorComponent],
  providers: [AllDoctorsService, AllPatientsService, AddPatientToDoctorService, UpdateDoctorService, DeleteDoctorService, AllListService, CreateDoctorService, DoctorPaginateService]
})
export class DoctorModule { }
