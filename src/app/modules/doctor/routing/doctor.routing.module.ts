import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DOCTOR_ROUTES } from './doctor.routes'

@NgModule({
    imports: [RouterModule.forChild(DOCTOR_ROUTES)],
    exports: [RouterModule]
})
export class DoctorRoutingModule {}
