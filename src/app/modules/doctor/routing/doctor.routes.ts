import { Routes } from '@angular/router';
import { NotFoundComponent } from '../../shared/components/not-found/not-found.component';
import { DoctorCreateComponent } from '../components/doctor-create/doctor-create.component';
import { DoctorListComponent } from '../components/doctor-list/doctor-list.component';
import { DoctorSearchComponent } from '../components/doctor-search/doctor-search.component';
import { MainComponent } from '../components/main/main.component';

export const DOCTOR_ROUTES: Routes = [
    { 
        path: '', 
        component: MainComponent,
        children: [
            { 
                path: '', 
                redirectTo: '/doctors/list',
                pathMatch: 'full'
            },
            { 
                path: 'list', 
                component: DoctorListComponent 
            },
            { 
                path: 'create', 
                component: DoctorCreateComponent 
            },
            {
                path: 'search',
                component: DoctorSearchComponent
            },
            { 
                path: '**', 
                component: NotFoundComponent 
            }
        ]
    }
];

