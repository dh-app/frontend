import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class DeleteDoctorService extends AppService{
    constructor(private _httpClient: HttpClient){
        super('doctors', _httpClient);
    }

    public deleteDoctor(doctorId: number): Observable<any> {
        return this.httpClient.delete(`${this.url}?doctorId=${doctorId}`);
    }
}