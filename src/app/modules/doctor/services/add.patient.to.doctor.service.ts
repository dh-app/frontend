import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class AddPatientToDoctorService extends AppService{
    constructor(private _httpClient: HttpClient) {
        super('doctors/add-patients', _httpClient);
    }

    public addPatientToDoctor(body): Observable<any> {
        return this.httpClient.put(this.url, body);
    }
}