import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class AllDoctorsService extends AppService{
    constructor(private _httpClient: HttpClient){
        super('doctors', _httpClient);
    }

    public getAllDoctors(): Observable<any> {
        return this.httpClient.get(this.url);
    }
}