import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()

export class DoctorPaginateService extends AppService{
    constructor(private _httpClient: HttpClient){
        super('doctors/search', _httpClient);
    }

    public getDoctorsPaginate(limit:number, page:number, name:string = '', lastname:string = ''): Observable<any> {
        return this.httpClient.get(`${this.url}?limit=${limit}&page=${page}&name=${name}&lastName=${lastname}`);
    }
}