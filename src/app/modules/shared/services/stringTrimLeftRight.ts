import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class StringTrimLeftRight {
    constructor() {

    }

    public sanitizeString(text: string): string {
        if (text.length > 0) {
            text = this.trimInicio(text);
            text = this.trimFinal(text);
        }

        return text;
    }

    private trimInicio(string) {
        var response = '';
        if (string.length > 0) {
            for (let i = 0; i < string.length; i++) {
                const element = string.charAt(i);
                if (element !== ' ') {
                    response = string.substr(i);
                    break;
                }
            }
        }

    return response;
    }

    private trimFinal(string) {
        var response = '';
        if (string.length > 0) {
            for (let i = string.length - 1; i > 0; i--) {
                const element = string.charAt(i);
                if (element !== ' ') {
                    response = string.substr(0, i + 1);
                    break;
                }
            }
        }

        return response;
    }
}