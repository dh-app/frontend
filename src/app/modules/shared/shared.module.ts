import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarModule } from 'primeng/sidebar';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ButtonModule } from 'primeng/button';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { DataViewModule } from 'primeng/dataview';
import { DialogModule } from 'primeng/dialog';
import { CardModule } from 'primeng/card';
import { CalendarModule } from 'primeng/calendar';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';

import { NotFoundComponent } from './components/not-found/not-found.component';
import { FooterComponent } from './components/footer/footer.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ListboxModule } from 'primeng/listbox';

@NgModule({
  declarations: [NotFoundComponent, FooterComponent, NavbarComponent],
  imports: [
    CommonModule,
    PanelMenuModule,
    SidebarModule
  ],
  exports: [
    TableModule, 
    NavbarComponent, 
    FooterComponent, 
    PanelMenuModule, 
    SidebarModule, 
    HttpClientModule, 
    ButtonModule,
    TooltipModule,
    DataViewModule,
    DialogModule,
    CardModule,
    ReactiveFormsModule,
    CalendarModule,
    InputTextModule,
    InputTextareaModule,
    InfiniteScrollModule,
    ListboxModule
  ]
})
export class SharedModule { }
