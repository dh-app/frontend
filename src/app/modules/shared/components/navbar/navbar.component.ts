import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  
  public showMenu: boolean;
  public items: MenuItem[];

  constructor() { 
    this.showMenu = false;
  }

  ngOnInit() {
    this.composeMenu();
  }

  private composeMenu(): void {
    this.items = [
      {
        label: 'Hospitals',
        icon: 'pi pi-fw pi-home',
        items: [
          {
            label: 'Register hospital',
            icon: 'pi pi-fw pi-plus',
            routerLink: '/hospitals/create'
          },
          {
            label: 'Show hospitals',
            icon: 'pi pi-fw pi-home',
            routerLink: '/hospitals/list'
          }
        ]
      },
      {
        label: 'Patients',
        icon: 'pi pi-fw pi-users',
        items: [
          {
            label: 'Register patient',
            icon: 'pi pi-fw pi-plus',
            routerLink: '/patients/create'
          },
          {
            label: 'Show patients',
            icon: 'pi pi-fw pi-users',
            routerLink: '/patients/list'
          },
          {
            label: 'Search patients',
            icon: 'pi pi-fw pi-search',
            routerLink: '/patients/search'
          }
        ]
      }, 
      {
        label: 'Doctors',
        icon: 'pi pi-fw pi-users',
        items: [
          {
            label: 'Register doctor',
            icon: 'pi pi-fw pi-plus',
            routerLink: '/doctors/create'
          },
          {
            label: 'Show doctors',
            icon: 'pi pi-fw pi-users',
            routerLink: '/doctors/list'
          },
          {
            label: 'Search doctors',
            icon: 'pi pi-fw pi-search',
            routerLink: '/doctors/search'
          }
        ]
      }, 
      {
        label: 'Specialities',
        icon: 'pi pi-fw pi-briefcase',
        items: [
          {
            label: 'Register speciality',
            icon: 'pi pi-fw pi-plus',
            routerLink: '/specialities/create'
          },
          {
            label: 'Show specialities',
            icon: 'pi pi-fw pi-briefcase',
            routerLink: '/specialities/list'
          }
        ]
      }  
    ];
  }

}
