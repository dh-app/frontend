import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { SpecialitiesListComponent } from './components/specialities-list/specialities-list.component';
import { SpecialitiesCreateComponent } from './components/specialities-create/specialities-create.component';
import { SpecialityRoutingModule } from './routing/speciality.routing.module';
import { AllListService } from './services/all.list.service';
import { SpecialityCreateService } from './services/speciliaty.create.service';
import { MainSpecialityComponent } from './components/main-speciality/main-speciality.component';
import { SpecialitiesUpdateComponent } from './components/specialities-update/specialities-update.component';
import { DeleteSpecialityService } from './services/speciality.delete.service';
import { SpecialityUpdateService } from './services/speciality.update.service';


@NgModule({
  declarations: [SpecialitiesListComponent, SpecialitiesCreateComponent, MainSpecialityComponent, SpecialitiesUpdateComponent],
  imports: [
    CommonModule,
    SharedModule,
    SpecialityRoutingModule
  ],
  providers: [AllListService, SpecialityCreateService, DeleteSpecialityService, SpecialityUpdateService]
})
export class SpecialityModule { }
