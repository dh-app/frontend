import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class SpecialityCreateService extends AppService{
    constructor(private _httpClient: HttpClient){
        super('specialities', _httpClient);
    }

    public createSpeciality(body: any): Observable<any> {
        return this.httpClient.post(this.url, body);
    }
}