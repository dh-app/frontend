import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class SpecialityUpdateService extends AppService{
    constructor(private _httpClient: HttpClient) {
        super('specialities', _httpClient);
    }

    public updateSpeciality(body: any): Observable<any> {
        return this.httpClient.put(this.url, body);
    }
}