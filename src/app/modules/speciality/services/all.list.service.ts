import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';
import { map } from 'rxjs/operators';

@Injectable()
export class AllListService extends AppService{
    constructor(private _httpClient: HttpClient){
        super('specialities', _httpClient);
    }

    public listSpecialities(): Observable<any>{
        return this.httpClient.get(this.url);
    }
}