import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/services/app.service';

@Injectable()
export class DeleteSpecialityService extends AppService {
    constructor(private _httpClient: HttpClient){
        super('specialities', _httpClient);
    }

    public deleteSpecialities(specialityId: number): Observable<any> {
        return this.httpClient.delete(`${this.url}?specialityId=${specialityId}`);
    }
}