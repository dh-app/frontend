export interface SpecialityModel {
    id: number;
    name: string;
    description: string;
    createdDate: string;
    updatedDate: string;
    createdBy: string;
    updatedBy: string;
}