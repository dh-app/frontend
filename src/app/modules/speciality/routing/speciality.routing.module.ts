import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SPECIALITY_ROUTES } from './speciality.routes';

@NgModule({
    imports: [RouterModule.forChild(SPECIALITY_ROUTES)],
    exports: [RouterModule]
})
export class SpecialityRoutingModule {}
