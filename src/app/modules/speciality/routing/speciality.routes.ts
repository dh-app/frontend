import { Routes } from "@angular/router";
import { NotFoundComponent } from "../../shared/components/not-found/not-found.component";
import { MainSpecialityComponent } from "../components/main-speciality/main-speciality.component";
import { SpecialitiesCreateComponent } from "../components/specialities-create/specialities-create.component";
import { SpecialitiesListComponent } from "../components/specialities-list/specialities-list.component";

export const SPECIALITY_ROUTES: Routes = [
    { 
        path: '', 
        component: MainSpecialityComponent,
        children: [
            { 
                path: '', 
                redirectTo: '/specialities/list',
                pathMatch: 'full' 
            },
            { 
                path: 'list', 
                component: SpecialitiesListComponent 
            },
            {
                path: 'create',
                component: SpecialitiesCreateComponent
            },
            {
                path: '**',
                component: NotFoundComponent
            }
        ] 
    }
];
