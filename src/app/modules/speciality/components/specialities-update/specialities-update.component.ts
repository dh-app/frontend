import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { StringTrimLeftRight } from 'src/app/modules/shared/services/stringTrimLeftRight';
import { SpecialityModel } from '../../models/SpecialityModel';
import { SpecialityUpdateService } from '../../services/speciality.update.service';

@Component({
  selector: 'app-specialities-update',
  templateUrl: './specialities-update.component.html',
  styleUrls: ['./specialities-update.component.scss']
})
export class SpecialitiesUpdateComponent implements OnInit {
  public updateForm: FormGroup;
  @Input() public speciality: SpecialityModel;
  private subscription: Subscription;

  constructor(private _formBuilder: FormBuilder,
              private _sanatizeService: StringTrimLeftRight,
              private _updateSpecialityService: SpecialityUpdateService) { 
    this._buildForm();
  }

  private _buildForm(): void {
    this.updateForm = this._formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      description: ['', [Validators.required, Validators.maxLength(255)]]
    });
  }

  ngOnInit(): void {
    this.updateForm.patchValue({
      name: this.speciality.name,
      description: this.speciality.description
    });
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscribe: Subscription): void {
    if (subscribe) {
      subscribe.unsubscribe();
      subscribe = null;
    }
  }

  public onSubmit(): void {
    (this.updateForm.valid) ? this._subscribe() : alert('Incorrect values');
  }

  private _subscribe(): void {
    let body = {
      specialityId: this.speciality.id,
      name: this._sanatizeService.sanitizeString(this.updateForm.get("name").value),
      description: this._sanatizeService.sanitizeString(this.updateForm.get("description").value),
      user: "USER-HARDCODE"
    }
    this.subscription = this._updateSpecialityService.updateSpeciality(body).subscribe(() => {
      alert('Correct');
    }, () => {
      alert('Error');
    });
  }

  public inputValidator(controlName: string): boolean {
    return this.updateForm.get(controlName).invalid && (this.updateForm.get(controlName).touched || this.updateForm.get(controlName).dirty);
  }

  public inputRequiredValidator(controlName: string): boolean {
    return this.updateForm.get(controlName).errors.required;
  }

  public inputMaxLengthValidator(controlName: string): boolean {
    return this.updateForm.get(controlName).errors.maxlength;
  }

  public get formValidator(): boolean {
    return this.updateForm.valid === false;
  }

}
