import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { StringTrimLeftRight } from 'src/app/modules/shared/services/stringTrimLeftRight';
import { SpecialityCreateService } from '../../services/speciliaty.create.service';

@Component({
  selector: 'app-specialities-create',
  templateUrl: './specialities-create.component.html',
  styleUrls: ['./specialities-create.component.scss']
})
export class SpecialitiesCreateComponent implements OnInit {

  public createSpeciality: FormGroup;
  private subscription: Subscription;

  constructor(private _formBuilder: FormBuilder,
    private _sanatizeString: StringTrimLeftRight,
    private _specialityCreate: SpecialityCreateService) {
      this.buildForm();
  }

  private buildForm(): void {
    this.createSpeciality = this._formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(255)]],
      description: ['', [Validators.required, Validators.maxLength(255)]]
    });
  }

  ngOnInit() {

  }

  public onSubmit(): void {
    (this.createSpeciality.valid) ? this._subscribe() : alert('Incorrect values');
  }

  private _subscribe(): void {
    let body = {
      name: this._sanatizeString.sanitizeString(this.createSpeciality.get("name").value),
      description: this._sanatizeString.sanitizeString(this.createSpeciality.get("description").value),
      user: "USER_HARDCODE"
    };
    this.subscription = this._specialityCreate.createSpeciality(body).subscribe(() => {
      alert('Created');
    }, () => {
      alert('Error');
    });
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

  public inputValidator(controlName: string): boolean {
    return this.createSpeciality.get(controlName).invalid && (this.createSpeciality.get(controlName).touched || this.createSpeciality.get(controlName).dirty);
  }

  public inputRequiredValidator(controlName: string): boolean {
    return this.createSpeciality.get(controlName).errors.required;
  }

  public inputMaxLengthValidator(controlName: string): boolean {
    return this.createSpeciality.get(controlName).errors.maxlength;
  }

  public get formValidator(): boolean {
    return this.createSpeciality.valid === false;
  }
}
