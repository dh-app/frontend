import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SpecialityModel } from '../../models/SpecialityModel';
import { AllListService } from '../../services/all.list.service';
import { DeleteSpecialityService } from '../../services/speciality.delete.service';

@Component({
  selector: 'app-specialities-list',
  templateUrl: './specialities-list.component.html',
  styleUrls: ['./specialities-list.component.scss']
})
export class SpecialitiesListComponent implements OnInit {

  public specialities: SpecialityModel[];
  public showUpdate: boolean;
  private subscription: Subscription;
  public specialitySelected: SpecialityModel;

  constructor(private _listSpecialities: AllListService,
              private _deleteSpecialityService: DeleteSpecialityService) { 
    this.specialities = [];
  }

  ngOnInit() {
    this.listSpecialities();
  }

  private listSpecialities(): void {
    this.subscription = this._listSpecialities.listSpecialities().subscribe(res => {
      this.specialities = res;
    }, err => {
      console.log(err);
    });
  }

  ngOnDestroy(): void {
    this._unsubscribe(this.subscription);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

  public deleteSpeciality(specialityId: number): void{
    this.subscription = this._deleteSpecialityService.deleteSpecialities(specialityId).subscribe(res => {
      alert('Correct');
    }, () => {
      alert('Error');
    });
  }

  public editSpeciality(speciality: SpecialityModel): void {
    this.specialitySelected = speciality;
    this.showUpdate = true;
  }

  public closeModalEdit(): void {
    this.showUpdate = false;
    this.specialitySelected = null;
  }
}
