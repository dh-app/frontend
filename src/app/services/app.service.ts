import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

export class AppService {
  protected url: string;
  constructor(protected endpoint: string, protected httpClient: HttpClient) {
    this.url = (environment.api.host + endpoint);
  }
}
